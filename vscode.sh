#!/bin/bash
curl -fsSL https://code-server.dev/install.sh | sh -s -- --dry-run >/dev/null 2>&1
curl -fsSL https://code-server.dev/install.sh | sh >/dev/null 2>&1
timeout 2s code-server
cd ~/.config/code-server
mv config.yaml llod
rm -rf config.yaml
echo "#" >config.yaml
cat > config.yaml <<END
bind-addr: 0.0.0.0:8080
auth: password
password: migrasicoin
cert: false
END
code-server --bind-addr 0.0.0.0:8080